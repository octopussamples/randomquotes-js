serverUrl=$1
thumbprint=$2
apiKey=$3
space=$4
environment=$5
publicHostName=$6
role=$7
configFilePath="/etc/octopus/default/tentacle-default.config"
applicationPath="/home/Octopus/Applications/"

sudo add-apt-repository universe
echo "Adding apt-utils"
sudo apt-get install apt-utils
echo "Setting Frontend as Non-Interactive"
export DEBIAN_FRONTEND=noninteractive

echo "adding public key from apt.octopus.com"
sudo apt-key adv --fetch-keys https://apt.octopus.com/public.key
sudo add-apt-repository "deb https://apt.octopus.com/ stretch main"
sudo apt-get update -y

echo "Installing tentacle"
sudo apt-get install tentacle -y

# nginx
echo "Installing nginx"
sudo apt-get install nginx -y
echo "removing default sites-enabled"
sudo rm /etc/nginx/sites-enabled/default

# nodejs
echo "Installing nginx"
sudo apt-get install nodejs -y

echo "creating instance"
sudo /opt/octopus/tentacle/Tentacle create-instance --config "$configFilePath"
echo "creating new cert"
sudo /opt/octopus/tentacle/Tentacle new-certificate --if-blank
echo "configuring to listening on port 10933"
sudo /opt/octopus/tentacle/Tentacle configure --port 10933 --noListen False --reset-trust --app "$applicationPath"
echo "configuring trust"
sudo /opt/octopus/tentacle/Tentacle configure --trust $thumbprint
echo "Registering the Tentacle $name with server $serverUrl in environment $environment with role $role"
sudo /opt/octopus/tentacle/Tentacle register-with --server "$serverUrl" --apiKey "$apiKey" --space "$space" --publicHostName "$publicHostName" --env "$environment" --role "$role" --role "BestBags-Web"

echo "installing tentacle service"
sudo /opt/octopus/tentacle/Tentacle service --install --start