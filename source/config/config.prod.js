// config.js
const config = {
    port: #{Project.Nodejs.Port},
    appVersion: '#{Octopus.Release.Number}',
    environmentName: '#{Octopus.Environment.Name}'
};

module.exports = config;
